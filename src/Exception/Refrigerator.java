package Exception;

import java.util.ArrayList;

public class Refrigerator {
	private int size;
	private ArrayList<String> things;
	private boolean checkfull ;
	
	public Refrigerator(int x){
		things = new ArrayList<String>(x);
		checkfull = true;
	}
	
	public void  put(String stuff) throws FullException{
		if(things.size()==6){
			throw new FullException("Can't put");
			
		}
		things.add(stuff);
		
	}
	public void setState(){
		this.checkfull = false;
	}
	public String toString(){
		String tmp= "";
		for(int k =0;k<things.size();k++){
			tmp= tmp+things.get(k)+"\n";
		}
		return tmp;
		
	}
}
