package Exception;

import java.sql.Ref;

public class Main {

	public static void main(String[] args) {
		Refrigerator refri = new Refrigerator(6);
		try{
			refri.put("Chocolate");
			refri.put("Fish");
			refri.put("Water");
			refri.put("Apple");
			refri.put("Kiwi");
			refri.put("Banana");
			refri.put("Milk");
			
		}
		catch(FullException e){
			System.err.println("Error: " + e.getMessage());
		}
		
		System.out.println(refri.toString());
	}

}
