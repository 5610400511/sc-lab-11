package HashMap;

import java.util.HashMap;

public class WordCounter {
	String message;
	int count;
	HashMap<String, Integer> wordCount;
	int num;

	public WordCounter(String message) {
		this.message = message;
		wordCount = new HashMap<String, Integer>();

	}

	public int hasWord(String word) {
		if (wordCount.containsKey(word)) {
			return wordCount.get(word);
		}
		return 0;

	}

	public void count() {
		String a[] = message.split(" ");
		for (String str : a) {

			if (wordCount.containsKey(str)) {

				wordCount.put(str, wordCount.get(str) + 1);
			} else {
				wordCount.put(str, 1);

			}

		}

	}

}